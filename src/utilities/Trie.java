package utilities;

import java.util.HashMap;
import java.util.Map;

public class Trie {
    

	private TrieNode root;
	private TrieNode pointerLazyResearch;
 
    public Trie() {
        root = new TrieNode();
    }
 
    // Inserts a word into the trie.
    public void insert(String[] strings) {
        HashMap<String, TrieNode> children = root.children;
 
        for(int i=0; i<strings.length; i++){
            String s = strings[i];
 
            TrieNode t;
            if(children.containsKey(s)){
                    t = children.get(s);
            }else{
                t = new TrieNode(s);
                children.put(s, t);
            }
 
            children = t.children;
 
            //set leaf node
            if(i==strings.length-1)
                t.isLeaf = true;    
        }
    }
 


 
    public TrieNode searchNode(String[] str){
        return searchNode(str,root);
    }
    
    public TrieNode searchNode(String[] str, TrieNode startNode){
    	if(startNode==null) startNode=root;
        Map<String, TrieNode> children = startNode.children; 
        TrieNode t = null;
        for(int i=0; i<str.length; i++){
            String s = str[i];
            if(children.containsKey(s)){
                t = children.get(s);
                children = t.children;
            }else{
                return null;
            }
        }
 
        return t;
    }
    
    // Returns if there is any word in the trie
    // that starts with the given prefix.
    public boolean startsWith(String[] prefix) {
        if(searchNode(prefix) == null) 
            return false;
        else
            return true;
    }
    
  
    
    // Returns if the word is in the trie.
    public boolean startsWith_LazyModeSearch(String prefix) {
    	String[] array = new String[1];
    	array[0]=prefix;
    	TrieNode t = searchNode(array,this.pointerLazyResearch);
    	if(t == null) 
            return false;
        else{
        	this.pointerLazyResearch=t;
            return true;
        }
    }
    
    public boolean isLazyModeSearchDone(){
    	return this.pointerLazyResearch.isLeaf;
    }
    
    public void closeLazyMode(){
    	this.pointerLazyResearch=null;
    }
    
    
    public boolean startsWith(String prefix) {
    	String[] array = new String[1];
    	array[0]=prefix;
        if(searchNode(array) == null) 
            return false;
        else
            return true;
    }
    
    
    // Returns if the word is in the trie.
    public boolean search(String[] strings) {
        TrieNode t = searchNode(strings);
 
        if(t != null && t.isLeaf) 
            return true;
        else
            return false;
    }
    
    
//    private StringBuilder printTrie(TrieNode t, StringBuilder sb){
//    	HashMap<String, TrieNode> children = t.children;
//    	if(children.isEmpty()) {
//    		sb.append("*\n");
//    		return sb;
//    	}
//    	
//    	for(String s:children.keySet()){
//    		sb.append(s+"==>");
//    		TrieNode t_child = children.get(s);
//    		while(t_child!=null){
//    			sb.append(t_child.s);
//    			printTrie(t_child,sb);
//    		}
//    		
//    	}
//    	
//    	return sb;
//    }
//    
//    @Override
//	public String toString() {
//    	StringBuilder result = printTrie(root, new StringBuilder());
//    	return result.toString();
//	}

}