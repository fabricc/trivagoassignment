package utilities;

import java.util.HashMap;

class TrieNode {
    String s;
    HashMap<String, TrieNode> children = new HashMap<String, TrieNode>();
    boolean isLeaf;
 
    public TrieNode() {}
 
    public TrieNode(String s){
        this.s = s;
    }
}