package explorer;

public class RatingStorer {
	public Double getValue() {
		return value;
	}

	public Integer getAmount_of_ratings() {
		return amount_of_ratings;
	}

	Double value=0.0;
	Integer amount_of_ratings=0;
	
	public RatingStorer(Double initialValue){
		this.addValue(value);
	}
	
	public void addValue(Double value){
		if(value!=null && value>-1) {
			this.value=this.value+value;
			amount_of_ratings++;
		}
	}
	
	

}
