package explorer;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import datamodel.Hotel;
import datamodel.Review;

public class Explorer {
	List<Hotel> hotels;
	
	public Explorer(List<Hotel> hotels){
		this.hotels=hotels;
	}
	
	public List<ReportEntry> getReportScoreFromListOfTerms(List<String> topic){
		HashSet<String> setOfTopics = new HashSet<String>(topic);
		List<ReportEntry> result = new LinkedList<ReportEntry>();
		for(Hotel hotel: this.hotels){
			ReportEntry re = new ReportEntry(hotel.getName(),hotel.getId(),topic);
			for(Review rev: hotel.getReviews()){
				calculateScoreFromReview(re,rev,setOfTopics);
				
			}
			result.add(re);
		}
		return result;
	}
	
	private ReportEntry calculateScoreFromReview(ReportEntry re, Review rev, HashSet<String> setOfTopics){
		Boolean match_found = false;
		Double score = 0.0;
		Double temp_score =0.0;
		Integer first_match = 0;
		List<String> semantic_occurences=new LinkedList<String>();
		for(int i=0; i<rev.getTextSize(); i++){
			String word = rev.getWord(i);
			if(word.equals(".")||word.equals("!")||i==rev.getTextSize()-1){
				if(match_found) {
					score+=temp_score;
					re.addSemtimentOccurences(semantic_occurences);
				}
				
				semantic_occurences = new LinkedList<String>();
				temp_score=0.0;
				match_found=false;
				if(first_match==1){
					re.setRatings(rev.getRatings());
					first_match++;
				}
				continue;
			}
			if(setOfTopics.contains(word)){
				match_found=true;
				first_match++;
				re.countFoundWords(word);
			}
			

			temp_score+=rev.getSemanticValue(i);
			String term= rev.getSemanticWord(i);
			
			if(term!=null) semantic_occurences.add(term);
			
			
		}
		
		re.addScore(score);
		return re;
		
	}

}
