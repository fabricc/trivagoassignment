package explorer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class SynonymApi {

	private final String USER_AGENT = "Mozilla/5.0";
	private final String app_id = "5879d31a";
	private final String app_key = "b6d16c3b3bfcd7b4e05db6e67ee13a92";
	private String url = "https://od-api.oxforddictionaries.com:443/api/v1/entries/en/";

	public static void main(String[] args) throws Exception {

		SynonymApi http = new SynonymApi();

		System.out.println("Testing 1 - Send Http GET request");
		http.sendGet();


	}
	
	public List<String> retrieveSynonyms(String synonym){
		this.url +=synonym+"/synonyms";
		String response=null;
		try {
			response = this.sendGet();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			return null;
		}
		return extractSynonymsFromOxfordDictionariesResponse(response);
	}
	
	private LinkedList<String> extractSynonymsFromOxfordDictionariesResponse(String response){
		
		JsonReader jsonReader = Json.createReader(new StringReader(response));
		
		JsonArray results = jsonReader.readObject().getJsonArray("results");
		jsonReader.close();
		LinkedList<String> result = new LinkedList<String>();
		
		for(int i=0; i<results.size(); i++){
			JsonArray lexicalEntries = ((JsonObject)results.get(i)).getJsonArray("lexicalEntries");
			for(int j=0; j<lexicalEntries.size(); j++){
				JsonArray entries = ((JsonObject)lexicalEntries.get(j)).getJsonArray("entries");
				for(int y=0; y<entries.size(); y++){
					JsonArray senses = ((JsonObject)entries.get(y)).getJsonArray("senses");
					for(int z=0; z<senses.size(); z++){
						JsonArray syn = ((JsonObject)senses.get(z)).getJsonArray("synonyms");
						for(int w=0; w<syn.size(); w++){
							String text = ((JsonObject)syn.get(w)).getString("text");
							//System.out.println(text);
							String[] array = text.split(" ");
							if(array.length>1) continue;
							result.add(text);
						}
					}
				}
			}
		}
		
		return result;
	}

	// HTTP GET request
	private String sendGet() throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestProperty("app_id",app_id);
        con.setRequestProperty("app_key",app_key);
		con.setRequestProperty("User-Agent", USER_AGENT);


		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		//System.out.println(response.toString().replaceAll("\\s+", " "));
		return response.toString();

}
}