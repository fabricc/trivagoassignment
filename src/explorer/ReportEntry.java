package explorer;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ReportEntry implements Comparable<ReportEntry>{
	
	String name;
	String id;
	Double score=0.0;
	HashMap<String, Integer> sentiment_occurrencies;
	HashMap<String, Integer> found_words;
	HashMap<String, RatingStorer> ratings;

	public Double getScore() {
		return score;
	}


	public void addScore(Double score) {
		this.score += score;
	}
	
	public void setRatings(HashMap<String, Double> rat){
		for(Entry<String, Double> e: rat.entrySet()){
			RatingStorer rs = this.ratings.get(e.getKey());
			if(rs==null) this.ratings.put(e.getKey(), new RatingStorer(e.getValue()));
			else rs.addValue(e.getValue());
			
		}
	}
	
	public ReportEntry(String name, String id,List<String> topic) {
		this.name = name;
		this.id = id;
		this.sentiment_occurrencies=new HashMap<String, Integer>();
		this.ratings = new HashMap<String,RatingStorer>();
		this.found_words = new HashMap<String, Integer>();
		for(String word: topic){
			this.found_words.put(word, 0);
		}
	}
	
	public void addSemtimentOccurence(String term){
		Integer value = this.sentiment_occurrencies.get(term);
		if(value==null) this.sentiment_occurrencies.put(term, 1);
		else this.sentiment_occurrencies.put(term, value+1);
	}
	
	public void addSemtimentOccurences(List<String> terms){
		for(String term: terms){
			this.addSemtimentOccurence(term);
		}
	}

	
	@Override
	public String toString() {
		String result = "ReportEntry: HotelName=" + name + ", id=" + id + ", score=" + score + "\n";
		result+="Occurrencies Query Entries:";
		for(Entry<String,Integer> e: this.found_words.entrySet()){
			result+=e.getKey()+"("+e.getValue()+") ";
		}
		result+="\n";
		result+="Sentiment Expression Counts:";
		HashMap<String,Integer> map = sortByValue(this.sentiment_occurrencies);
		for(Entry<String, Integer> e:map.entrySet()){
			result+=e.getKey()+"=>";
			result+=e.getValue()+" ";
		}
		
		result+="\nAverage Ratings ";
		for(Entry<String, RatingStorer> e: this.ratings.entrySet()){
			result+=e.getKey()+":";
			result+=e.getValue().getValue()/e.getValue().getAmount_of_ratings()+"  ";
			result+="Total voters:"+e.getValue().getAmount_of_ratings()+"  ";
		}
		
		return result+"\n\n";
	}
	
    private static HashMap<String,Integer>sortByValue(HashMap<String,Integer>unsortMap) {


        List<Map.Entry<String, Integer>> list =
                new LinkedList<HashMap.Entry<String, Integer>>(unsortMap.entrySet());


        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
			public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });


        HashMap<String,Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }




        return sortedMap;
    }

	
    @Override
    public int compareTo(ReportEntry re) {       
        if(this.score>re.getScore()) return -1;
        else if(this.score<re.getScore()) return 1;
        return 0;
    }


	public void countFoundWords(String word) {
		Integer count=this.found_words.get(word);
		this.found_words.put(word, count+1);
	}


}
