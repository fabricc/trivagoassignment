package loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import datamodel.Hotel;
import datamodel.Review;
import datamodel.SemanticExpression;
import utilities.SupportMethods;
import utilities.Trie;

public class Loader {
	String path = null;
	final String hotels_file_name = "reviews";
	final String semantic_file_name = "semantics.json";
	
	List<JsonObject> json_hotels = new LinkedList<JsonObject>();
	List<Hotel> hotels = new LinkedList<Hotel>();
	JsonObject semantic = null;
	HashMap<String, SemanticExpression> SemanticMap;
	Trie trie;
	
	private void load_files(String path_to_data){
		this.path=path_to_data;
		Path path = Paths.get(this.path);
		InputStream fis = null;
		JsonReader jsonReader = null;
		JsonObject jsonObject = null;
		
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
		    for (Path file: stream) {
		    	String s = file.getFileName().toString();
		    	
		    	if(s.contains(hotels_file_name)){
		    		fis = new FileInputStream(file.toFile().getAbsolutePath());
		    		jsonReader = Json.createReader(fis);
		    		
		    		jsonObject = jsonReader.readObject();
		    		
		    		jsonReader.close();
		    		fis.close();
		    		
		    		json_hotels.add(jsonObject);
		    	}else if(s.equals(semantic_file_name)){
		    		
		    		fis = new FileInputStream(file.toFile().getAbsolutePath());
		    		jsonReader = Json.createReader(fis);
		    		
		    		jsonObject = jsonReader.readObject();
		    		
		    		jsonReader.close();
		    		fis.close();
		    		semantic = jsonObject;
		    	}
		        
		    }
		} catch (IOException | DirectoryIteratorException x) {
		    // IOException can never be thrown by the iteration.
		    // In this snippet, it can only be thrown by newDirectoryStream.
		    System.err.println(x);
		}
	}
	
	
	private void generateSemanticMap(){
		SemanticMap=new HashMap<String,SemanticExpression>();
		trie = new Trie();
		populateSemanticMap("positive");
		populateSemanticMap("negative");
		populateSemanticMap("intensifier");
	}
	
	private void populateSemanticMap(String type){
		JsonArray words = (JsonArray)semantic.get(type);
		int numerical_type = 0; //0 positive, 1 negative, 2 multiplier
		Boolean multiplier = false;
		if(type.equals("negative")) numerical_type = 1;
		else if(type.equals("intensifier")) {
			numerical_type = 2;
			multiplier=true;
		}
		
		
		for(Object o: words){
			JsonObject jo = (JsonObject)o;
		
			Double value;
			if(numerical_type!=2){
				value=jo.getJsonNumber("value").doubleValue();
				if(numerical_type==1) value=0-value;
			}else value=jo.getJsonNumber("multiplier").doubleValue();
			
			String phrase = jo.getString("phrase");
			SemanticMap.put(phrase,new SemanticExpression(phrase,value, multiplier));
			trie.insert(phrase.split(" "));
		}
		
		
	}
	
	
	private static String cleanText(String text){
		text = text.replaceAll("showReview\\([0-9]+,\\s'full'\\);", "");
		text = text.replaceAll("[\"'\u2018\u2019\u201c\u201d]", "");
		text = text.replaceAll("\\.\\.\\.", " ");
		text = text.replaceAll("\\(", " ");
		text = text.replaceAll("\\)", " ");
		text = text.replaceAll("\\.", " . ");
		text = text.replaceAll("\\,", " , ");
		text = text.replaceAll("\\:", " : ");
		text = text.replaceAll("\\;", " ; ");
		text = text.replaceAll("\\!+", " ! ");
		text = text.replaceAll("\\?+", " ? ");
		text = text.replaceAll("\\s+", " ");
		text = text.toLowerCase();
		return text;
	}
	
	private static String cleanTitle(String title){
		if(title.equals("")) return title;
		String text = title.replaceAll("\u201c", "");
		text = text.replaceAll("\u201d", "");
		return text+".";

	}
	
	private void generateHotelList(){
		for(JsonObject jo:json_hotels){
			jo=jo.getJsonObject("HotelInfo");
			String name = jo.getString("Name", null);
			String id = jo.getString("HotelID");
			Hotel hotel = new Hotel(name,id);
			this.hotels.add(hotel);
		}
	}
	
	private Review extractStopWordsFromReview(Review rev, HashSet<String> stopwords){
		
		for(int i=0; i<rev.getTextSize(); i++){
			if(!rev.isWordASemanticExpression(i)){
				String word = rev.getWord(i);
				if(stopwords.contains(word))rev.removeWord(i);
			}
		}
		
		return rev;
	}
	
	private void parseReviews(){
		String csvFile = this.path+File.separator+"stopwords.csv";
        String line = "\n";
        HashSet<String> stop_words = new HashSet<String>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
               stop_words.add(line);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    
		for(int i=0; i<this.hotels.size(); i++){
			JsonArray reviews = this.json_hotels.get(i).getJsonArray("Reviews");
			Hotel hotel = this.hotels.get(i);
			for(int j=0; j<reviews.size(); j++){
				JsonObject json_review = (JsonObject)reviews.get(j);
				String content = json_review.getString("Content");
				String title = cleanTitle(json_review.getString("Title",""));
				content = cleanText(title+content);
				Review rev = new Review();
				this.generateTokendizedTextReview(rev, content);
				this.extractStopWordsFromReview(rev, stop_words);
				this.addReviewRatings(rev, json_review.getJsonObject("Ratings"));
				hotel.addReview(rev);
			}
		}
	}
	
	private Review addReviewRatings(Review rev, JsonObject ratings){
		for (String key: ratings.keySet()) {
		    rev.setRating(key, Double.valueOf(ratings.getString(key)));
		}
		return rev;
	}
	

	
	private Review generateTokendizedTextReview(Review rev, String text){
		String[] splitted_text = text.split(" ");
		
		int start = 0;
		int end = 0;
		int temp_end = 0;
		
		boolean isPartOfBiggerExpression = false;
		boolean found = false;
		for(int i=0; i<splitted_text.length; i++){
			
			isPartOfBiggerExpression=true;
			found = false;
			start=i;
			temp_end=i;
			while(isPartOfBiggerExpression && temp_end<splitted_text.length){
				if(!trie.startsWith_LazyModeSearch(splitted_text[temp_end])) isPartOfBiggerExpression=false;
				if(isPartOfBiggerExpression && trie.isLazyModeSearchDone()){
					end=temp_end;
					found=true;
				}
				temp_end++;
			}
			trie.closeLazyMode();
			if(found){
				String exp = SupportMethods.StringFromArray(splitted_text,start,end);
				SemanticExpression se = SemanticMap.get(exp);
				rev.addExpression(exp, se);
				i=end;}
			else rev.addExpression(splitted_text[i]);
	}
		return rev;
	}
	
	
	
	public Loader(String path_to_data){
		load_files(path_to_data);
		generateSemanticMap();
		generateHotelList();
		parseReviews();
	}
	
	public List<Hotel> getHotelsWithReviews(){
		return this.hotels;
	}
	
	public static void main(String[] args){
		
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			Loader l = new Loader("C:\\Users\\Fabio\\workspace\\TrivagoAssignment\\data");
			String content = "This is the content to write into file\n";

			fw = new FileWriter("C:\\Users\\Fabio\\workspace\\TrivagoAssignment\\data\\out");
			bw = new BufferedWriter(fw);
			bw.write(content);
			
			for(int i=0;i<l.hotels.size();i++){
				bw.write(l.hotels.get(i).getId());
				bw.write("\n");
				bw.write("\n");
				for(Review r: l.hotels.get(i).getReviews()){
					bw.write(r.getText());
					bw.write("\n");
				}
			}

			System.out.println("Done");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

	}
		

	}


