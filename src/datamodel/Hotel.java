package datamodel;

import java.util.LinkedList;
import java.util.List;

public class Hotel {

	private String name;
	private String id;
	private List<Review> reviews;
	
	public Hotel(String name, String id){
		this.id=id;
		this.name=name;
		this.reviews=new LinkedList<Review>();
	}
	
	public int getNumberReviews(){
		return this.reviews.size();
	}
	
	public List<Review> getReviews(){
		return this.reviews;
	}
	
	public void addReview(Review review){
		this.reviews.add(review);
	}
	
	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}
}
