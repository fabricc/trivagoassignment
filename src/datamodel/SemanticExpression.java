package datamodel;

public class SemanticExpression {
	
	String name;
	double value;
	Boolean multiplier = false;
	
	public SemanticExpression(String name, double value, Boolean multiplier) {
		this.name = name;
		this.value = value;
		this.multiplier = multiplier;
	}
	
	public SemanticExpression(int value) {
		this.value = value;

	}

	public double getValue() {
		return value;
	}
	
	public void multiply(Double multiplier){
		this.value=value*multiplier;
	}
	
	public Boolean isMultiplier() {
		return multiplier;
	}
	
	
	public SemanticExpression getCopty(){
		return new SemanticExpression(this.name,this.value,this.multiplier);
	}



}
