package datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class Review {

	public HashMap<String, Double> getRatings() {
		return ratings;
	}


	LinkedList<String> text;
	ArrayList<SemanticExpression> sentiment_values;
	HashMap<String,Double> ratings;
	
	public Review(){
		text=new LinkedList<String>();
		sentiment_values=new ArrayList<SemanticExpression>();
		this.ratings=new HashMap<String,Double>();
	}
	
	public boolean isWordASemanticExpression(int i){
		if(sentiment_values.get(i)!=null) return true;
		else return false;
	}
	
	
	public String getWord(int i){
		return this.text.get(i);
	}
	
	public String getSemanticWord(int i){
		SemanticExpression se = this.sentiment_values.get(i);
		if(se==null) return null;
		return this.getWord(i);
	}
	
	public Double getSemanticValue(int i){
		SemanticExpression se = sentiment_values.get(i);
		if(se!=null) return se.getValue();
		return 0.0;
	}
	
	public String getText(){
		StringBuilder result = new StringBuilder();
		int i=0;
		for(String s: text){
			result.append(s);
			if(this.isWordASemanticExpression(i))result.append("("+this.sentiment_values.get(i).getValue()+")");
			result.append(" ");
			i++;
		}
		return result.toString();
	}
	
	public int getTextSize(){
		return this.text.size();
	}
	
	public void removeWord(int i){
		this.text.remove(i);
		this.sentiment_values.remove(i);
	}
	
	public void addExpression(String exp){
		text.add(exp);
		sentiment_values.add(null);
		
		int indexLastInserted = text.size()-2;
		if(indexLastInserted<0) return;
		SemanticExpression lastMultiplier = sentiment_values.get(indexLastInserted);
		while(lastMultiplier!=null && indexLastInserted>-1){
			if(lastMultiplier.isMultiplier()){
				sentiment_values.set(indexLastInserted, null);
			}else break;
			if((--indexLastInserted)==-1) break;
			lastMultiplier = sentiment_values.get(indexLastInserted);
		}
		
	}
	
	public void addExpression(String exp, SemanticExpression se){
		se = se.getCopty();
		text.add(exp);
		sentiment_values.add(se);
		
		if(!se.isMultiplier()){
			int indexLastInserted = text.size()-2;
			if(indexLastInserted<0) return;
			SemanticExpression lastMultiplier = sentiment_values.get(indexLastInserted);
			while(lastMultiplier!=null && indexLastInserted>-1){
				if(lastMultiplier.isMultiplier()){
					Double multiplier = lastMultiplier.getValue();
					se.multiply(multiplier);
					sentiment_values.set(indexLastInserted, null);
				}else break;
				if((--indexLastInserted)==-1) break;
				lastMultiplier = sentiment_values.get(indexLastInserted);
			}
		}
	}
	
	
	public void setRating(String rating, Double value){
		this.ratings.put(rating, value);
	}
	
	
	

	
}
