import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import explorer.Explorer;
import explorer.ReportEntry;
import explorer.SynonymApi;
import loader.Loader;

public class Main {

	public static void main(String[] args) {
		
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type the path of the folder containing your reviews, your semantic and your stopwords");
		String path = scanner.nextLine();
		Loader l = new Loader(path);
		Explorer e = new Explorer(l.getHotelsWithReviews());
		
		System.out.println("Type the topic you are interested in and press Enter:");
		String input = scanner.nextLine();
		System.out.println("You wrote:"+input);
		List<String> inputs = new LinkedList<String>();
		inputs.add(input);
		SynonymApi sapi = new SynonymApi();
		List<String> synonyms = sapi.retrieveSynonyms(input);
		String synonym = "text";
		if(synonyms==null || synonym.length()==0) System.out.println("No synonyms found!");
		else while(true){
				System.out.println("In order to increase the precision of your research you could add one or more of this synonyms to your query:");
				System.out.println(synonyms);
				System.out.println("Type one of the words of the list and press Enter or type OK and press Enter when you are done");
				synonym = scanner.nextLine();
				if(synonym.equals("OK")) break;
				if(!synonyms.remove(synonym)) System.out.println("You typed a word not in the list!");
				inputs.add(synonym.toLowerCase());
				System.out.println("Your inputs are:"+inputs);
			}
		scanner.close();
		System.out.println("Executing research...");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("\n");
		List<ReportEntry> report = e.getReportScoreFromListOfTerms(inputs);
		Collections.sort(report);
		for(ReportEntry re: report){
			System.out.println(re);
		}
	}

}
