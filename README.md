# Trivago Assignment#

### What is this repository for? ###

This repo contains the assignment requested by Trivago from the position of Software Engineer. The project was coded in Java and can be executed downloading the jar in the root running  "java -jar trivagoassignment.jar". When the app starts, a path containing the dataset is requested. The folder should contain:

* One or more JSON files containing the reviews. The filename of the files should contain the word "review"
* A JSON file called "semantics.json" containing the semantic expressions and their values.
* A CSV file called "stopwords.csv" containing a list of stopword not useful for the research.

### Project Structure ###
This software was designed having Scalability has a major concern. In order to keep the execution of research queries fast, the whole process was divided into two steps, loading and exploring. The class Loader manages the parsing of the files and their elaboration. In a real-life scenario they can be executed only the first time. The class Explorer instead handles the research of the topic inserted by the user. among the reviews of the hotels.

In the specific, the tasks executed by the Explorer are:

1. Parsing of the files including hotel reviews and a list of words called semantic expressions and their score.
2. The semantic expressions are stored in two data structure: a map and a trie. The map contains as a key the expressions in the form of strings and as a value the object SemanticExpression that keeps track of their score. The Trie was used to speed up the process of recognition of the semantic expression during the step 5, assisting the identification of expressions made by more than one word.
3. Generation of a list of Hotel instance containing the names and ids
4. For every hotel, a list of Review objects is generated. A Review is a class containing an array, a list, and a map. The list contains the text of the review split in tokens and a token can be either a single word, a semantic expression made by one or more words or punctuation characters. The array has the same size of the list and for every cell contains a pointer to the object SemanticExpression that stores the score of the expression in the array. If the expression is in the same position of the list is not an expression, the cell in the array contains a null pointer, e.g. list=[the, room, was, clean] array=[null,null,null,SemanticExpression pointer]. The map stores the ratings provided by the users in the file.
5. The texts of the reviews are cleaned by redundant or useless punctuation characters and stored in the Reviews instance. The Trie and the SemanticMap are used to identify the semantic expressions.
6. Multiplier are immediately applied to the adjacent SemanticExpression,
7. In order to make the research faster, stopwords are extracted from the text of the reviews.
8. The list of Hotels and Reviews are passed to the Explorer.

In the following, the execution steps of the explorer are listed:


1. The Explorer requests to the user to insert a topic.
2. A list of synonyms of that words are retrieved. For this purpose, the API provided in https://developer.oxforddictionaries.com/documentation was used.
3. The user picks the synonyms that can help in refining his research.
4. The Explorer go trough all the reviews and idenfies the ones mentioning one of the words provided by the user. It calculates the score present in the sentence where the topic is mentioned. A sentences is referred as a set of word between two dots or exclamation marks.
5. For every Hotel, it is created an associated ReportEntry that keeps track of the score of that hotel. In addition, ReportEntry contains three maps. They are used to count the occurencies of the semantic expression found, the occurrencies of the words provided as input and the average of the ratings
6. The output is provided displayng all the ReportEntry sorted by score.